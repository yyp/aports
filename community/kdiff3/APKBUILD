# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-other
pkgname=kdiff3
pkgver=1.10.7
pkgrel=1
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://apps.kde.org/kdiff3/"
pkgdesc="A file and folder diff and merge tool"
license="GPL-2.0-or-later "
makedepends="
	boost-dev
	extra-cmake-modules
	kconfig5-dev
	kcoreaddons5-dev
	kcrash5-dev
	kdoctools5-dev
	ki18n5-dev
	kparts5-dev
	kwidgetsaddons5-dev
	qt5-qtbase-dev
	samurai
	"
checkdepends="xvfb-run"
_repo_url="https://invent.kde.org/sdk/kdiff3.git"
source="$pkgname-$pkgver-1.tar.xz::https://download.kde.org/stable/kdiff3/kdiff3-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	xvfb-run ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
1b6c90cc64f3b60ecfbc0d7ecb68190ee844e747b9dbc599249f5f3cfd6c07e4ccce9bc2ec116ac7ab470d4e9862f096154533792b13f3d0135a97370549ac8b  kdiff3-1.10.7-1.tar.xz
"
