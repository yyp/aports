# Maintainer: Simon Rupf <simon@rupf.net>
pkgname=py3-flask-caching
_pkgname=flask_caching
pkgver=2.2.0
pkgrel=0
pkgdesc="Flask caching support"
url="https://flask-caching.readthedocs.io/"
arch="noarch"
license="BSD-3-Clause"
depends="python3 py3-cachelib py3-flask"
makedepends="py3-gpep517 py3-installer py3-setuptools py3-wheel"
checkdepends="py3-asgiref py3-redis py3-pytest"
subpackages="$pkgname-pyc"
source="https://files.pythonhosted.org/packages/source/${_pkgname:0:1}/$_pkgname/$_pkgname-$pkgver.tar.gz"
builddir="$srcdir"/$_pkgname-$pkgver
options="!check" # depends on nonfree redis features

build() {
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
}

check() {
	python3 -m venv --clear --without-pip --system-site-packages .testenv
	.testenv/bin/python3 -m installer .dist/*.whl
	.testenv/bin/python3 -m pytest
}

package() {
	python3 -m installer -d "$pkgdir" \
		.dist/*.whl
}

sha512sums="
cf38dbaf7337c6be45aa59aa398705fbd6968a567c84d707d5caaac22c2e10fe1dc5a157d5c896e6b95220b983c7e066c80cc631177e4e3e43677348900bd204  flask_caching-2.2.0.tar.gz
"
