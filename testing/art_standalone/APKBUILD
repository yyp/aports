# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=art_standalone
pkgver=0_git20240307
pkgrel=0
_commit="c3055ca258fc726e5fa9fd2aa1d9899a5c548e58"
pkgdesc="A standalone version of Dalvik with Art build in"
url="https://gitlab.com/android_translation_layer/dalvik_standalone"
arch="x86_64 aarch64 armv7"
license="Apache-2.0"
depends="wolfssl-jni"
# bash is used by /usr/bin/dx
depends_dev="
	$pkgname=$pkgver-r$pkgrel
	bash
	"
case "$CARCH" in
	aarch64|armv7) depends_dev="$depends_dev vixl-dev" ;;
esac
makedepends="$depends_dev
	bionic_translation-dev
	bsd-compat-headers
	expat-dev
	icu-dev
	java-common
	libbsd-dev
	libcap-dev
	libunwind-dev
	lz4-dev
	meson
	openjdk8-jdk
	openssl-dev
	python3
	valgrind-dev
	wolfssl-jni-dev
	xz-dev
	zip
	zlib-dev
	"
subpackages="$pkgname-dev $pkgname-dbg"
source="https://gitlab.com/android_translation_layer/art_standalone/-/archive/$_commit/art_standalone-$_commit.tar.gz"
options="!check" # No tests
builddir="$srcdir/art_standalone-$_commit"

prepare() {
	default_prepare

	# Hack
	ln -s /usr/bin/python3 python
	export PATH="$PATH:$PWD"
}

build() {
	make PREFIX=/usr ____LIBDIR=lib
}

package() {
	DESTDIR="$pkgdir" make \
		____PREFIX="$pkgdir"/usr \
		____INSTALL_ETC="$pkgdir"/etc \
		____LIBDIR=lib \
		install
}

dev() {
	default_dev

	mkdir -p "$subpkgdir"/usr/lib/java
	mv "$pkgdir"/usr/lib/java/core-all_classes.jar "$subpkgdir"/usr/lib/java
	mv "$pkgdir"/usr/lib/java/dx.jar "$subpkgdir"/usr/lib/java
	mkdir -p "$subpkgdir"/usr/bin
	mv "$pkgdir"/usr/bin/dx "$subpkgdir"/usr/bin/dx
}

sha512sums="
330d2767e67282c84161eb05b33d4099c7565067904e0cc9b7df79756e0cbf30f3d7fedf9290c3540441a11c6a3889f43f0e49f96949cc72db92926fde2d5383  art_standalone-c3055ca258fc726e5fa9fd2aa1d9899a5c548e58.tar.gz
"
