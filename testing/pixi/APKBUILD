# Contributor: Orhun Parmaksız <orhunparmaksiz@gmail.com>
# Maintainer: Orhun Parmaksız <orhunparmaksiz@gmail.com>
pkgname=pixi
pkgver=0.20.1
pkgrel=0
pkgdesc="A package management and workflow tool"
url="https://github.com/prefix-dev/pixi"
# !s390x: nix crate fails to build
# !armhf: openssl fails to build
arch="all !s390x !armhf"
license="BSD-3-Clause"
makedepends="
	cargo
	cargo-auditable
	openssl-dev
	perl
	"
subpackages="
	$pkgname-doc
	$pkgname-bash-completion
	$pkgname-zsh-completion
	$pkgname-fish-completion
	"
# https://github.com/prefix-dev/pixi/issues/821
options="net !check"
source="$pkgname-$pkgver.tar.gz::https://github.com/prefix-dev/pixi/archive/v$pkgver.tar.gz"

prepare() {
	default_prepare
	cargo fetch --target="$CTARGET" --locked
	mkdir -p completions/
}

build() {
	cargo auditable build --frozen --release
	local _completion="target/release/$pkgname completion"
	$_completion --shell bash > "completions/$pkgname"
	$_completion --shell fish > "completions/$pkgname.fish"
	$_completion --shell zsh  > "completions/_$pkgname"
}

package() {
	install -Dm 755 "target/release/$pkgname" -t "$pkgdir/usr/bin"
	install -Dm 644 README.md -t "$pkgdir/usr/share/doc/$pkgname"
	install -Dm 644 LICENSE -t "$pkgdir/usr/share/licenses/$pkgname"
	install -Dm 664 "completions/$pkgname" -t "$pkgdir/usr/share/bash-completion/completions/"
	install -Dm 664 "completions/$pkgname.fish" -t "$pkgdir/usr/share/fish/vendor_completions.d/"
	install -Dm 664 "completions/_$pkgname" -t "$pkgdir/usr/share/zsh/site-functions/"
}

sha512sums="
4dc393e0086fb0bf8f8b086877368eca5bba8a17d154f97a9f2bc8077c51552b622c35b9dbd9bb721d849ad034ec12774a7cd13aa25e583bf67b3553b850ab0a  pixi-0.20.1.tar.gz
"
